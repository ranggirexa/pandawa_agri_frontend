import React, { useState } from "react";
import axios from 'axios'
import { useNavigate } from 'react-router-dom'
import "./loginform.css"
import { Link } from 'react-router-dom'


const LoginForm = () => {

    const [username, setUsername]=useState("")
    const [password, setPassword]=useState("")
    const navigate =useNavigate()
    
    const saveUser = async (e) => {
        e.preventDefault()
        try {
           const response = await axios.post('http://localhost:5000/logins',{
                username,password
            })
            const token = response?.data.access_token;
            navigate("/home")
        } catch (error) {
            console.log(error);
        }
    }

    const [popupStyle, showPopup] = useState("hide")

    // const popup = () => {
    //     showPopup("login-popup")
    //     setTimeout(() => showPopup("hide"), 3000)
    // }

    return (
        // <div className="cover">
        //     <h1>Login</h1>
            // <input type="text" placeholder="username" name="" id="" />
            // <input type="password" placeholder="password" name="" id="" />

        //     <div className="login-btn" onClick={popup }>Login</div>

        //     <p className="text">Or login using</p>

        //     <div className="alt-login">
        //         <div className="facebook"></div>
        //         <div className="google"></div>
        //     </div>

        //     <div className={popupStyle}>
        //         <h3>Login Failed</h3>
        //         <p>Username or password incorect</p>
        //     </div>

        // </div>

        <div className="cover">
            <h1>Login</h1>
            <form onSubmit={saveUser}>
                <div className="field">
                    <label className="label">username</label>
                    <div className="control">
                        <input type="text" className="input" value={username} onChange={(e)=>setUsername(e.target.value)} placeholder='username'/>
                    </div>
                </div>
                <div className="field">
                    <label className="label">Password</label>
                    <div className="control">
                        <input type="password" className="input" value={password} onChange={(e)=>setPassword(e.target.value)} placeholder='password'/>
                    </div>
                </div>
                <div className="field">

                        <button type='submit' className='button is-success' >submit</button>

                </div>
            </form>
            <Link to={'add'} className="login-btn">Register</Link>

        </div>

    )

}

export default LoginForm