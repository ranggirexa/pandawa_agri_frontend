import React, {useState, useEffect} from 'react'
import axios from "axios"
import { Link } from 'react-router-dom'

const UserList = () => {
  const [users, setUser] = useState([])

  useEffect(() => {
    getUsers()
  },[])

  const getUsers = async () => {

    await axios.get('http://localhost:5000/users').then(resp => {
      setUser(resp.data.data[0]);
      console.log(resp.data.data[0]);

    })
    .catch(function (error) {
      if (error.response) {
        // Request made and server responded
        if (error.response.data.message === "Unauthorized") {
            // setUser(error.response.data.message)
            setUser('error');
      }
      } 
    });
    // console.log('mesagenya ',response.data);
  }
console.log("set user",users);

if (users == 'error') {
  return (
    <h5>Unauthorized</h5>
  )
}else{
  return (
    <div className="columns mt-5 is-centered">
        <div className="column is-half">
          <Link to={'add'} className="button is-success">Tambah</Link>
          <table className='table is-striped is-fullwidth' >
            <thead>
              <tr>
                <th>No</th>
                <th>Name</th>
                <th>Role</th>
              </tr>
            </thead>
            <tbody>
              {users.map((user,index)=> (
                <tr key={user.id}>
                  <td> {index +1 } </td>
                  <td> {user.username} </td>
                  <td> {user.role} </td>
                </tr>
                ) )}
            </tbody>
          </table>
        </div>
    </div>
  )
}

}

export default UserList